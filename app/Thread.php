<?php

namespace App;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\Markdown\Facades\Markdown;

class Thread extends Model
{
  protected $fillable = ['user_id', 'subject', 'body', 'slug', 'tags', 'best_reply_id'];

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function replies()
  {
    return $this->hasMany('App\Reply');
  }


  public function tags()
  {
   		/*belongsToMany(
    	'ModelName', 
    	'pivot_table', 
    	'foreign_key_defined_model', 
   	 	'foreign_key_joined_model'
     )*/
     return $this->belongsToMany('App\Tag', 'thread_tag', 'thread_id', 'tag_id');
   }

   public function getShortDescription()
   {
     return Str::words($this->body, 20, '...');
   }



   /*
    Now you want show what is the  *bestReply* in the show page, we need id of the Reply
    *best_reply_id* 
   */
    //belongsTo relationship for reply
    public function bestReplayRelationship()
    {
      return $this->belongsTo('App\Reply', 'best_reply_id');
    }

    //this return the bestReply
    public function returnBestReply()
    {
      return $this->bestReplayRelationship;
    }

    //check if reply is the bestReply or not 
    public function isItBestReply($reply)
    {
      if($bestReply = $this->returnBestReply()){
        return $bestReply->id === $reply->id;
      }
      return false;
    }

    //check if thread has bestReply or not 
    public function hasBestReply()
    {
      return !is_null($this->best_reply_id);
    }

    public function markDown($key) 
    {
      echo Markdown::convertToHtml($key);
    }
  }
