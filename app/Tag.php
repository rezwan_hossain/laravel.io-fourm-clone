<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function threads()
    {
    	/*belongsToMany(
    	'ModelName', 
    	'pivot_table', 
    	'foreign_key_defined_model', 
   	 	'foreign_key_joined_model'
		)*/
    	return $this->belongsToMany('App\Thread', 'thread_tag', 'tag_id', 'thread_id');
    }
}
