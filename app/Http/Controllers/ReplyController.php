<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Reply;
use App\Notifications\ReplyNotification;
use App\Notifications\BestReplyNotification;

class ReplyController extends Controller
{
    public function store()
    {
      request()->validate([
        'thread_id' => 'min:1',
        'body'    => 'required',  
    ]);

    $reply = Reply::create([
        'user_id' => Auth::user()->id,
        'thread_id' => request('thread_id'),
        'body'	  => request('body'),
    ]);

    // implement ReplyNotification for reply 
    /*
        if thread->user_id notEqual to reply->user_ui
        then notify the user who created the thread
    */
    if($reply->thread->user_id != $reply->user_id){
        $reply->thread->user->notify( new ReplyNotification($reply));
    }

    return redirect()->back();
}

public function bestReply(Reply $reply)
{
    $reply->thread->update(['best_reply_id' => $reply->id]);
    
    // notify the reply->user for the bestReply
    $reply->user->notify(new BestReplyNotification($reply));

    return redirect()->back();
}
}
