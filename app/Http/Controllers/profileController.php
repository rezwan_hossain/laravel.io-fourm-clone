<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use LasseRafn\InitialAvatarGenerator\InitialAvatar;
use App\User;

class profileController extends Controller
{
    public function avatar($username)
    {
    	$user =  User::where('name', $username)->firstOrFail();
    	$avatar = new InitialAvatar();

    	return $avatar
	           ->name($user->name)
	           ->background('#2C3E50')
	           ->color('#ffffff')
	           ->size(100)
	           ->generate()
	           ->stream('png', 100);
    }
}
