<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;

use App\Thread;
use App\Tag;
use App\Reply;

class ThreadController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        if(request('tag')){
            $threads = Tag::where('name', request('tag'))->firstOrFail()->threads;
        } else {
             $threads = Thread::latest()->paginate(10);
        }
        $req = request('tag');
        $tags = Tag::orderBy('name')->get();
        return view('overview', compact('threads', 'tags', 'req'));
    }

    public function create()
    {
        $tags = Tag::all();
    	return view('CreateThread', compact('tags'));
    }

    public function show($slug)
    {
        $thread = Thread::where('slug', $slug)->first();
    	return view('show', compact('thread'));
    }

    public function store()
    {
    	
    	// Thread::create(request()->validate([
    	// 	'subject' => 'required',
    	// 	'body'	  => 'required',
    	// 	'slug'	  => 'min:3'
    	// ]));

    	 /*$validatedData = request()->validate([
    	 	'user_id' => 'min:1',
    	 	'subject' => 'required',
    		'body'	  => 'required',
    		'slug'	  => 'min:3',
    		'tags' 	  => 'required|array'
    	 ]); 
    	 $validatedData['user_id'] = Auth::user()->id;
    	 $validatedData['slug'] = Str::slug(request('subject'), '-');

    	 Thread::create($validatedData);
        dd(request()->tags);*/

 
        request()->validate([
            'user_id' => 'min:1',
            'subject' => 'required',
            'body'    => 'required',
            'slug'    => 'min:3',
            'tags'    => 'required|array|min:1|exists:tags,id'
        ]);
        $thread = Thread::create([
            'user_id' => Auth::user()->id,
            'subject' => request('subject'),
            'body'	  => request('body'),
            'slug'	  => Str::slug(request('subject'), '-')
        ]);

        $thread->tags()->attach(request('tags'));

        return redirect('/forum');
    }

    
}
