<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    } 
    public function index()
    {
    	// dd(auth()->user()->notifications->first()->data['reply'] );
    	return view('dashbord', [
    		"notifications" => auth()->user()->notifications()->paginate(10)
    	]);
    }
    public function markAsRead($id)
    {
    	$Notification = auth()->user()->notifications->find($id);
    	if($Notification){
    		$Notification->markAsRead();
    	}

    	return redirect()->back();
    }
}
