<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/forum', 'ThreadController@index');
Route::get('/dashboard', 'NotificationController@index');
Route::get('/dashboard/{id}', 'NotificationController@markAsRead');
Route::get('/forum/{slug}', 'ThreadController@show');
Route::get('/create-thread', 'ThreadController@create');
Route::post('/create-thread', 'ThreadController@store');
Route::post('/forum/reply', 'ReplyController@store');
Route::post('/forum/{reply}/best', 'ReplyController@bestReply');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/{username}', 'UserController@show')->name('home');
Route::get('/avatar/{username}', 'profileController@avatar')->name('avatar');

