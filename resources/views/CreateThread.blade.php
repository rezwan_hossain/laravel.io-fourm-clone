@extends('layouts.layout')

@section('content')
<div class="border-b"> 
	<div class="container mx-auto">
		<h1 class="text-xl py-4">
			<a href="/forum">Forum</a>
			> Create thread
		</h1>
	</div>
</div>
<div class="container mx-auto pt-8 p-4 flex justify-center">
	<div class="w-2/4">
		<div class="p-4 border-2 rounded bg-gray-100">
			<form action="/create-thread" method="POST">
				@csrf
				<div class="mb-2">
					<label for="subject" class="uppercase">Subject</label>
					<input type="text" 
					class=" block w-full mt-2 h-12 p-2 border-2 rounded @error('subject') border-red-500 @enderror" 
					name="subject" value="{{ old('subject') }}">
					<span class="text-sm text-gray-600">Maximum 60 characterscharacters.</span> <br>
					@error('subject')
						<span class="text-sm text-red-600">{{ $errors->first('subject') }}</span>
					@enderror
				</div>
				<div class="mb-2">
					<label for="subject" class="uppercase">Body</label>
					<textarea name="body" id="textId" cols="30" rows="10" class="block w-full mt-2 p-2 border-2 rounded @error('body') border-red-500 @enderror" > {{ old('body') }} </textarea>	
					@error('body')
						<span class="text-sm text-red-600">{{ $errors->first('body') }}</span>
					@enderror
				</div> 
				<div class="mb-4">
					<label for="subject" class="uppercase" >Tags</label>
					<select name="tags[]" id="selectTag" class="block w-full mt-2 h-12 p-2 border-2 rounded @error('tags') border-red-500 @enderror" multiple >
					  @foreach($tags as $tag)
					  	<option value="{{$tag->id}}">{{$tag->name}}</option>
					  @endforeach
					</select>

					@error('tags')
						<span class="text-sm text-red-600">{{ $errors->first('tags') }}</span>
					@enderror
				</div>
				<div class=" flex justify-end items-center p-2">
					<a href="/forum" class="text-green-800 mr-4">Cancel</a>
					<button type="submit" class="bg-green-600 px-4 py-2 item-center rounded text-white text-base">Create Thread</button>
				</div>
			</form>
		</div>
	</div>
</div> 

@endsection