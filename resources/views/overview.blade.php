@extends('layouts.layout')

@section('content')
<div class="border-b"> 
	<div class="container mx-auto">
		<h1 class="text-xl py-4">Forum</h1>
	</div>
</div>
<div class="container mx-auto flex px-4 pt-4">
	<div class="w-3/4 pr-4"> 
		@forelse($threads as $thread)
			<div class="p-4 border rounded-md">
			<div class="">
				<a href="/forum/{{$thread->slug}}">
					<h4 class="flex justify-between text-xl font-bold break-all ">
						{{$thread->subject}}
						<span class="text-base font-normal">
							<svg class="inline text-gray-500 h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
								<path fill-rule="evenodd" d="M18 10c0 3.866-3.582 7-8 7a8.841 8.841 0 01-4.083-.98L2 17l1.338-3.123C2.493 12.767 2 11.434 2 10c0-3.866 3.582-7 8-7s8 3.134 8 7zM7 9H5v2h2V9zm8 0h-2v2h2V9zM9 9h2v2H9V9z" clip-rule="evenodd"/> 
							</svg>
							{{count($thread->replies)}}
						</span>
					</h4>
					<p class=" mt-2 text-gray-700">
						{{ $thread->getShortDescription() }}
					</p>
				</a>
			</div>
			<div class="flex justify-between item-center pt-5 text-sm ">
				<div class="flex">
					<div class="thread-info-avatar">
						<a href="/user/{{$thread->user->name}}">
							<img class="img-circle w-6 rounded-full mr-3" src="/avatar/{{$thread->user->name}}">
						</a>
					</div>
					<div>
						<a href="/user/{{$thread->user->name}}" class="text-green-500 mr-2">{{$thread->user->name}}</a>
						Posted   {{$thread->created_at->diffForHumans()}}
					</div>
					<div class="ml-4"> 
							{{-- <span class="bg-gray-400 text-gray-700 rounded px-2 py-1 mr-2 text-base ">
								Laravel
							</span> --}}
							@foreach($thread->tags as $tag)
								<a href="/forum?tag={{$tag->name}}">
									<span class="bg-gray-300 text-gray-700 rounded px-2 py-1 ">{{$tag->name}}</span>
								</a>
							@endforeach
							
							{{-- <span class="bg-gray-300 text-gray-700 rounded px-2 py-1 ">fase</span> --}}
						</div>
					</div>
					<div class="">
						@if($thread->hasBestReply())
							<span class="bg-green-400   uppercase rounded px-2 py-1 text-white">
								<svg class="inline w-4 h-4" viewBox="0 0 20 20" fill="currentColor">
									<path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
								</svg>							
									Have Solution
							</span>
						@endif
					</div>
				</div>
			</div>
			@empty
				<h1 class="">No relevent thread found</h1>
		@endforelse
		<div class="mt-16 mb-8">

			@if($req)
		
			@else
				{!! $threads->links() !!}
			@endif

		</div>
	</div>

		<div class="w-1/4 pl-4 pt-4">
			<a href="/create-thread" class=" bg-green-600 px-4 py-2 item-center rounded text-white text-base">Create Thread
			</a>
			<div class="mt-8"></div>
			<h3 class="text-xs font-bold tracking-wider uppercase text-gray-500">TAGS</h3>
			<ul class="pt-4 pl-4">
				<li>
					<a href="/forum">All</a>
				</li>

				@foreach($tags as $tag)
					<li class="mb-1">
						<a href="/forum?tag={{$tag->name}}">{{$tag->name}}</a>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
	@endsection