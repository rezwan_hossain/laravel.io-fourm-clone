@extends('layouts.layout')

@section('content')
<div class="container mx-auto flex px-4 pt-4 mt-8">
	<div class="w-1/5">
		<div class="flex flex-col mb-4 w-full ">
			<div class="mb-4">
				<a href="">
					<img class=" w-full" src="/avatar/{{$user->name}}">
				</a>
			</div>
			<h2 class="mb-4 text-2xl text-gray-900 mb-4"> {{$user->name}} </h2>
			<p class="text-gray-600 text-m mb-4">
				Joined {{ Carbon\Carbon::parse($user->created_at)->format('j F Y')}}
			</p>
		</div>
	</div>
	<div class="w-4/5 pl-8">
		<div class="flex">
			<div class="flex items-center mr-8 mb-8">
				<svg class="text-green-600 h-12 w-12 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
					<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path>
				</svg> 
				<p class="tex-xl uppercase">{{count($user->threads)}} threads</p>
			</div>
			<div class="flex items-center mr-8 mb-8">
				<svg class="text-green-600 h-12 w-12 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
					<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"></path>
				</svg> 
				<p class="tex-xl uppercase">{{ count($user->replies) }} replies</p>
			</div>
		</div>
		<div class="mb-4 mt-8  pb-2 ">
			<div class="border-b">
				<p class="py-4">Latest Threads</p>
			</div>
		</div>
		<div class="mb-8">
			@foreach($user->threads as $thread)
			<div class="p-4 border rounded-md">
				<div class="">
					<a href="/forum/{{$thread->slug}}">
						<h4 class="flex justify-between text-xl font-bold break-all ">
							{{$thread->subject}}
							<span class="text-base font-normal">
								<svg class="inline text-gray-500 h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
									<path fill-rule="evenodd" d="M18 10c0 3.866-3.582 7-8 7a8.841 8.841 0 01-4.083-.98L2 17l1.338-3.123C2.493 12.767 2 11.434 2 10c0-3.866 3.582-7 8-7s8 3.134 8 7zM7 9H5v2h2V9zm8 0h-2v2h2V9zM9 9h2v2H9V9z" clip-rule="evenodd"/> 
								</svg>
								{{count($thread->replies)}}
							</span>
						</h4>
						<p class=" mt-2 text-gray-700">
							{{ $thread->getShortDescription() }}
						</p>
					</a>
				</div>
				<div class="flex justify-between item-center pt-5 text-sm">
					<div class="flex">
						<div class="thread-info-avatar">
							<a href="/user/{{$thread->user->name}}">
								<img class="img-circle w-6 rounded-full mr-3" src="/avatar/{{$thread->user->name}}">
							</a>
						</div>
						<div>
							<a href="/user/{{$thread->user->name}}" class="text-green-500 mr-2">{{$thread->user->name}}</a>
							Posted   {{$thread->created_at->diffForHumans()}}
						</div>
						<div class="ml-4"> 
								{{-- <span class="bg-gray-400 text-gray-700 rounded px-2 py-1 mr-2 text-base ">
									Laravel
								</span> --}}
								@foreach($thread->tags as $tag)
								<a href="/forum?tag={{$tag->name}}">
									<span class="bg-gray-300 text-gray-700 rounded px-2 py-1 ">{{$tag->name}}</span>
								</a>
								@endforeach
								
								{{-- <span class="bg-gray-300 text-gray-700 rounded px-2 py-1 ">fase</span> --}}
							</div>
						</div>
						<div class="">
							<span class="bg-green-400   uppercase rounded px-2 py-1 text-white">
								<svg class="inline w-4 h-4" viewBox="0 0 20 20" fill="currentColor">
									<path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path>
								</svg>
								have solution
							</span>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	@endsection