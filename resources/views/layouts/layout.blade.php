<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>welcome page</title>
        <link href="{{ mix('css/app.css') }}" rel="stylesheet" />
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/choices.js@9.0.1/public/assets/styles/choices.min.css"
        />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/4.0.0/github-markdown.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.2.0/styles/monokai.min.css" integrity="sha512-z8wQkuDRFwCBfoj7KOiu1MECaRVoXx6rZQWL21x0BsVVH7JkqCp1Otf39qve6CrCycOOL5o9vgfII5Smds23rg==" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.2.0/highlight.min.js" integrity="sha512-TDKKr+IvoqZnPzc3l35hdjpHD0m+b2EC2SrLEgKDRWpxf2rFCxemkgvJ5kfU48ip+Y+m2XVKyOCD85ybtlZDmw==" crossorigin="anonymous"></script>
    </head>
    <body>
        <style>
            .markdown-body {
                box-sizing: border-box;
                min-width: 200px;
                max-width: 980px;
                margin: 0 auto;
                padding: 45px;
            }

            @media (max-width: 767px) {
                .markdown-body {
                    padding: 15px;
                }
            }
        </style>

        <header class="container mx-auto py-6">
            <div class="flex justify-between">
                <div class="">
                    <nav class="flex items-center">
                        <a href="/">
                            <img class="h-8 w-auto" src="https://laravel.io/images/laravelio-logo.svg" alt="Workflow logo" />
                        </a>
                        <div class="flex items-center mx-2">
                            <a href="/forum" class="mx-4">Forum</a>
                            <a href="#" class="mx-4">Articles</a>
                            <a href="#" class="mx-4">Pasebin </a>
                            <a href="#" class="mx-4">Chat</a>
                            <a href="#" class="mx-4">Event</a>
                            <a href="#" class="mx-4">Community</a>
                        </div>
                    </nav>
                </div>
                <div>
                    @auth
                    <div class=" block flex items-center">
   
                    		<a href="/dashboard" class="relative inline-block    mr-4 text-gray-400 rounded-full hover:text-gray-500 ">
                    			<span class="">
                    				<svg class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    				  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"></path>
                    				</svg> 
                    			</span>
                                @if(Auth::user()->unreadNotifications->count() == 0)
                                    <div></div>
                                    @else
                                        <span class="absolute text-center bg-red-500 rounded-full" 
                                            style="
                                                top: -5px;
                                                right: -2px;
                                                padding: 0.10em 0.55em;                   
                                                color: white;
                                                font-size: 8px;
                                                letter-spacing: -1px;                                      
                                            ">
                                            {{Auth::user()->unreadNotifications->count()}}
                                        </span>
                                @endif
                    		</a>          
						
                        <!-- Dropdown -->
                        <!-- use alpine js for the dropdown -->
                        <div class="block relative"  x-data="{ open: false }">
                            <div class="ml-3">
                                <div>
                                    <button @click="open = true" class="rounded-full border-2 items-center focus:outline-none">
                                        <img class="img-circle h-8 w-8 rounded-full" src="/avatar/{{ Auth::user()->name }}" />
                                    </button>
                                </div>
                            </div>
                            <!-- Dropdown Body -->
                            <div class="absolute right-0 mt-2 w-40 rounded-md shadow-lg" x-show.transition="open" x-show="open" @click.away="open = false " style="display: none;">
                                <div class=" py-1 rounded-md bg-white shadow-xs">
                                    <a href="/user/{{Auth::user()->name}}" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">Your Profile</a>
                                    <a href="/dashboard" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">Dashboard</a>
                                    <a
                                        href="{{ route('logout') }}"
                                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                        onclick="event.preventDefault();
										document.getElementById('logout-form').submit();"
                                    >
                                        Sign out
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endauth
                    @guest
                        <a href="{{ route('login') . '?previous=' . Request::fullUrl() }}" class="mx-4">Login</a>
                        <a href="/register" class="mx-4">Register</a>
                    @endguest
                </div>
            </div>
        </header>
        @yield('content')
    </body>
</html>
