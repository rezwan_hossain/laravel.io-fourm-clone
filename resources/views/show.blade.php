@extends('layouts.layout')

@section('content')
<div class="border-b">
	<div class="container mx-auto ">
		<h1 class="text-xl py-4"> <a href="/forum">Forum</a> > {{$thread->subject}}</h1>
	</div>	
</div>
<div class="container mx-auto flex  pt-4">
	<div class="w-3/4 ">
		<div class="border rounded">
			<div class="p-4 border-b text-sm">
				<div class="flex justify-between items-center  mr-2">
					<div class="flex">
						<div class="thread-info-avatar">
							<a href="/user/{{$thread->user->name}}">
								<img class="img-circle w-6 rounded-full mr-3" src="/avatar/{{$thread->user->name}}">
							</a>
						</div>
						<div class="text-sm">
							<a href="/user/{{$thread->user->name}}" class="text-green-900 mr-2">{{$thread->user->name}}</a>
							Posted  {{$thread->created_at->diffForHumans()}}
						</div>
					</div>
					<div class="ml-4">
						@foreach($thread->tags as $tag)
							<a href="/forum?tag={{$tag->name}}">
									<span class="bg-gray-300 text-gray-700 rounded px-2 py-1 ">{{$tag->name}}</span>
								</a>
						@endforeach
					</div>

				</div>
			</div>
			<div class="p-4">
				<div>
					<h1>{{$thread->markDown($thread->body)}}</h1>
				</div>
			</div>
			<div class="border-t">
				<i class="py-2 px-2 text-2xl" aria-hidden="true"></i>
			</div>
		</div>

		@foreach($thread->replies as $reply)
		<div class="mt-8">
			<div class=" {{ $thread->isItBestReply($reply) ? 'border-2 border-green-300' : 'border' }}  rounded">
				@if($thread->isItBestReply($reply))
					<div class="bg-green-600 text-white uppercase px-4 py-2 opacity-75">
                        Solution
                    </div>
				@endif
				<div class="p-4 border-b text-sm">
					<div class="flex justify-between items-center  mr-2">
						<div class="flex">
							<div class="thread-info-avatar">
								<a href="$">
									<img class="img-circle w-6 rounded-full mr-3" src="/avatar/{{$reply->user->name}}">
								</a>
							</div>
							<div class="text-sm">
								<a href="#" class="text-green-900 mr-2">{{$reply->user->name}}</a>
								replied  {{$reply->created_at->diffForHumans()}}
							</div>
						</div>
						<div class="ml-4"> 
							@if( Auth::id() == $thread->user_id)
								@if($thread->isItBestReply($reply))
									<div></div>
									@else
										<form method="POST" action="/forum/{{$reply->id}}/best">
											@csrf
											<span class="bg-green-300 text-gray-700 rounded px-2 py-1">
												<button type="submit">mark as solution</button>
											</span>
										</form>
								@endif
										
							@endif
						</div>

					</div>
				</div>
				<div class="p-4">
					<div>
						<h1>{{$thread->markDown($reply->body)}}</h1>
					</div>
				</div>
				<div class="border-t">
					<i class="py-2 px-2 text-2xl" aria-hidden="true"></i>
				</div>
			</div>
		</div>
		@endforeach
		<div class="my-8">
			@guest
			<p class="text-center text-gray-800 py-8">
				<a href="{{ route('login') }}" class="text-green-700" >Sign in </a> to participate in this thread!
			</p>
			@endguest
			@auth
			<label class="uppercase text-gray-700">Write a reply</label>
			<form action="/forum/reply" method="POST">
				@csrf
				<input type="hidden" name="thread_id" value="{{$thread->id}}">
				<textarea name="body" id="textId" cols="16" rows="6" class="block w-full mt-2 p-2 border-2 rounded" ></textarea>
				<div class="flex justify-between mt-4 items-center">
					<p class="text-sm">Please make sure you've read our Forum Rules before replying to this thread</p>
					<button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Reply</button>
				</div>
			</form>
			@endauth
		</div>

	</div>

	<div class="w-1/4 pr-4">
		<div class="flex flex-col mb-4 mt-8 w-full  items-center ">
			<div class="mb-4">
				<a href="/user/{{$thread->user->name}}">
					<img class="" src="/avatar/{{ $thread->user->name }}" />
				</a>
			</div>
			<h2 class="text-2xl text-gray-900 mb-4">{{ $thread->user->name }}</h2>
			<p class="text-sm mb-4">Joined {{ Carbon\Carbon::parse($thread->user->created_at)->format('j F Y')}}</p>
		</div>
	</div>
</div>
@endsection