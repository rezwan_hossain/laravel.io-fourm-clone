@extends('layouts.layout')

@section('content')
	<div class="min-h-screen flex items-center justify-center ">
		<div class="mt-8 mx-auto">
			<img src="https://laravel.io/images/laravelio.png" alt="" class=" max-w-2xl mx-auto mb-10 ">
			<p class="text-center text-4xl font-extrabold ">
				The Laravel Community Portal
			</p>
			<div class="flex items-center justify-center mt-12">
				
				<a href="/forum" class="bg-blue-500 hover:bg-blue-500 text-white font-bold py-4 px-4 rounded shadow-md">Visit The Forum</a>
			</div>
		</div>
	</div>
@endsection