@extends('layouts.layout')

@section('content')
	<div class="border-b">
		<div class="container mx-auto ">
			<h1 class="text-xl py-4"> Dashbord </h1>
		</div>
	</div>

	<div class="container mx-auto flex px-4 pt-4 mt-8">
		<div class="w-1/5"></div>
		<div class="w-4/5 pl-8">
			<div class="flex">
			<div class="flex items-center mr-8 mb-8">
				<svg class="text-green-600 h-12 w-12 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
					<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path>
				</svg> 
				<p class="tex-xl uppercase">{{ Auth::user()->threads->count() }} threads</p>
			</div>
			<div class="flex items-center mr-8 mb-8">
				<svg class="text-green-600 h-12 w-12 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
					<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"></path>
				</svg> 
				<p class="tex-xl uppercase">{{ Auth::user()->replies->count() }} replies</p>
			</div>
		</div>
		<div class="mb-4 mt-8  pb-2 ">
			<div class="border-b">
				<p class="py-4">Notifications</p>
			</div>
		</div>
			@foreach( Auth::user()->unreadNotifications as $notification)
				<div class="p-4 border rounded-md flex justify-between">
					<div class="">
						@if($notification->type === 'App\Notifications\ReplyNotification' )
							<a href="#"><b class="text-green-600">{{$notification->data['reply']}}</b> </a>
							reply your <a href="/forum/{{ $notification->data['thread'] }}" class="text-green-600">thread</a> 
							@else($notification->type === 'App\Notifications\BestReplyNotification')
								<p>your reply is the best reply</p>
						@endif	
					</div>				
					<div class="px-6">
						<a href="dashboard/{{$notification->id}}" class="text-green-600">mark as read</a>
					</div>				
				</div>
			@endforeach

			{!! $notifications->links() !!}
		</div>
		
	</div>
@endsection