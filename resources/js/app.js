import 'alpinejs';
import SimpleMDE from 'SimpleMDE';
import Choices from 'choices.js';
import hljs from 'highlight.js';
require('./bootstrap');

var simplemde = new SimpleMDE({ 
	element: document.getElementById("textId"),
	hideIcons: ["guide","ordered-list","unordered-list"],
	showIcons: ["code"],
});

// choice.js for multiple selector
if(document.getElementById('selectTag')){
	var textRemove = new Choices(document.getElementById('selectTag'), {
		delimiter: ',',
		editItems: true,
		maxItemCount: 3,
		removeItemButton: true,
	});
}

//highlight.js 
document.addEventListener('DOMContentLoaded', (event) => {
  document.querySelectorAll('pre code').forEach((block) => {
    hljs.highlightBlock(block);
  });
});
